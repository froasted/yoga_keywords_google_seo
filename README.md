# Yoga Search Analysis on Google Custom Search API
Analysis done on hottest yoga search keywords over a custom google search API with geography set to US.

## Requirements
The following packages are required to run the code :

**Python 3.7+**

- **pandas** (pip install pandas)

- **advertools** (pip install advertools)

- **google-api-python-client** (pip install google-api-python-client)

- **matplotlib** (pip install matplotlib)

- **plotly** (pip install plotly) 

- **seaborn** (pip install seaborn)

- **ipywidgets** (pip install ipywidgets)